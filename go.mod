module github.com/tionis/api

go 1.15

require (
	github.com/bwmarrin/discordgo v0.22.0
	github.com/gin-gonic/gin v1.6.3
	github.com/heroku/x v0.0.26
	github.com/keybase/go-logging v0.0.0-20200423195923-7a5ab2ef7dec
	github.com/lib/pq v1.8.0
	github.com/tionis/uni-passau-bot v0.1.4
	gopkg.in/tucnak/telebot.v2 v2.3.5
)
